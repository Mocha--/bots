module.exports = {
    root: true,
    parser: '@typescript-eslint/parser',
    parserOptions: {
        project: 'tsconfig.json',
        tsconfigRootDir: __dirname
    },
    plugins: [
        '@typescript-eslint',
    ],
    extends: [
        'airbnb-typescript'
    ],
    rules: {
        'comma-dangle': ['error', 'never'],
        'object-curly-spacing': ['error', 'never'],
        'object-curly-newline': 0,
        'arrow-parens': 0,
        'no-nested-ternary': 0,
        'max-len': 0,
        'consistent-return': 0,
        'curly': ['error', 'multi-or-nest'],
        'no-new': 0,
        'radix': ['error', 'as-needed'],
        'no-underscore-dangle': 0,
        'no-param-reassign': ['error', {props: false}],
        'no-await-in-loop': 0,
        'no-restricted-syntax': 0,
        'no-console': 0,

        /**
         * Import plugin
         */
        'import/prefer-default-export': 0,

        /**
         * Typescript plugin
         */
        '@typescript-eslint/no-use-before-define': 0,
        '@typescript-eslint/lines-between-class-members': 0,
        '@typescript-eslint/indent': 0
    }
};
