import chalk from 'chalk';
import {Direction, gridWidth, gridHeight} from './constants';
import {Robot} from './models';

export function placeRobot(robot: Robot, x: number, y: number, direction: Direction): Robot {
    if (!direction) {
        console.error(chalk.red('Error: Invalid direction'));
        return robot;
    }

    if (x < 0 || x >= gridWidth || y < 0 || y >= gridHeight) {
        console.error(chalk.red('Error: Cannot place robot out of grid'));
        return robot;
    }

    return {...robot, x, y, direction};
}

export function moveForward(robot: Robot): Robot {
    const {x, y, direction} = robot;

    if (direction === Direction.North) {
        if (y + 1 < gridHeight) return {...robot, y: y + 1};

        console.error(chalk.red('Error: Cannot move, reaching top edge'));
        return robot;
    }

    if (direction === Direction.East) {
        if (x + 1 < gridWidth) return {...robot, x: x + 1};

        console.error(chalk.red('Error: Cannot move, reaching right edge'));
        return robot;
    }

    if (direction === Direction.South) {
        if (y - 1 >= 0) return {...robot, y: y - 1};

        console.error(chalk.red('Error: Cannot move, reaching bottom edge'));
        return robot;
    }

    if (direction === Direction.West) {
        if (x - 1 >= 0) return {...robot, x: x - 1};

        console.error(chalk.red('Error: Cannot move, reaching left edge'));
        return robot;
    }

    console.error(chalk.red('Error: Cannot move, invalid direction'));
    return robot;
}

export function turnLeft(robot: Robot): Robot {
    const {direction} = robot;

    if (direction === Direction.North) return {...robot, direction: Direction.West};
    if (direction === Direction.East) return {...robot, direction: Direction.North};
    if (direction === Direction.South) return {...robot, direction: Direction.East};
    if (direction === Direction.West) return {...robot, direction: Direction.South};

    console.error(chalk.red('Error: Cannot turn left, invalid direction'));
    return robot;
}

export function turnRight(robot: Robot): Robot {
    const {direction} = robot;

    if (direction === Direction.North) return {...robot, direction: Direction.East};
    if (direction === Direction.East) return {...robot, direction: Direction.South};
    if (direction === Direction.South) return {...robot, direction: Direction.West};
    if (direction === Direction.West) return {...robot, direction: Direction.North};

    console.error(chalk.red('Error: Cannot turn right, invalid direction'));
    return robot;
}

export function report(robot: Robot): string {
    const {x, y, direction} = robot;
    const directionText: string = direction === Direction.North ? 'NORTH'
        : direction === Direction.East ? 'EAST'
        : direction === Direction.South ? 'SOUTH'
        : direction === Direction.West ? 'WEST'
        : null;

    return `${x},${y},${directionText}`;
}

export function drawGrid(robot: Robot) {
    const {x, y, direction} = robot;
    const columns: number[] = range(gridWidth);
    const rows: number[] = range(gridHeight);
    const horizontalCode = '\u2015';
    console.info(chalk.cyan('current grid:'));
    console.info(` ${columns.map(() => horizontalCode).join(' ')}`);

    rows.reverse()
        .forEach(rIdx => {
            const row: string = columns
                .map(cIdx => {
                    if (rIdx !== y || cIdx !== x) return '*';
                    if (direction === Direction.North) return chalk.greenBright('\u2191');
                    if (direction === Direction.East) return chalk.greenBright('\u2192');
                    if (direction === Direction.South) return chalk.greenBright('\u2193');
                    if (direction === Direction.West) return chalk.greenBright('\u2190');
                    return '*';
                })
                .join('|');

            console.info(`|${row}|`);
            console.info(` ${columns.map(() => horizontalCode).join(' ')}`);
        });

    console.info('');
}

function range(count: number, from: number = 0): number[] {
    return Array.from(Array(count).keys()).map<number>(elm => elm + from);
}
