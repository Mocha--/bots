import {Direction} from '../constants';

export interface Robot {
    direction: Direction;
    x: number;
    y: number;
}
