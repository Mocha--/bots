import {createInterface} from 'readline';
import chalk from 'chalk';
import {Direction, PLACE_COMMAND, MOVE_COMMAND, LEFT_COMMAND, RIGHT_COMMAND, REPORT_COMMAND, EXIT_COMMAND, NORTH, EAST, SOUTH, WEST} from './constants';
import {Robot} from './models';
import {placeRobot, moveForward, turnLeft, turnRight, drawGrid, report} from './utils';

const rl = createInterface({input: process.stdin, output: process.stdout});
let robot: Robot = {x: 0, y: 0, direction: null};

printHints();

rl.prompt();
rl.on('line', input => {
    try {
        const cmdSegments = input.trim().split(' ');
        const [cmd, args] = cmdSegments;

        if (cmd !== PLACE_COMMAND
            && cmd !== LEFT_COMMAND
            && cmd !== RIGHT_COMMAND
            && cmd !== MOVE_COMMAND
            && cmd !== REPORT_COMMAND
            && cmd !== EXIT_COMMAND
        ) throw new Error(`Command must be one of ${PLACE_COMMAND}, ${LEFT_COMMAND}, ${RIGHT_COMMAND}, ${MOVE_COMMAND}, ${REPORT_COMMAND}, ${EXIT_COMMAND}`);

        if (cmd === EXIT_COMMAND) {
            rl.close();
            process.exit(0);
        }

        if (cmd !== PLACE_COMMAND && !robot.direction) throw new Error('The Robot has to be placed first');

        if (cmd === PLACE_COMMAND) {
            if (args?.split(',').length !== 3) throw new Error('PLACE command is not in correct format. Please follow the format of PLACE {x},{y},{direction}');

            const [x, y, direction] = args.split(',');
            if (x === '' || (x !== '' && Number.isNaN(Number(x)))) throw new Error('X must be a number');
            if (y === '' || (y !== '' && Number.isNaN(Number(y)))) throw new Error('Y must be a number');
            if (direction !== NORTH && direction !== EAST && direction !== SOUTH && direction !== WEST) throw new Error(`Direction must be one of ${NORTH}, ${EAST}, ${SOUTH}, ${WEST}`);

            const dir = direction === NORTH ? Direction.North
                : direction === EAST ? Direction.East
                : direction === SOUTH ? Direction.South
                : direction === WEST ? Direction.West
                : null;

            robot = placeRobot(robot, Number(x), Number(y), dir);
            drawGrid(robot);
        }

        if (cmd === MOVE_COMMAND) {
            robot = moveForward(robot);
            drawGrid(robot);
        }

        if (cmd === LEFT_COMMAND) {
            robot = turnLeft(robot);
            drawGrid(robot);
        }

        if (cmd === RIGHT_COMMAND) {
            robot = turnRight(robot);
            drawGrid(robot);
        }

        if (cmd === REPORT_COMMAND) console.info(chalk.green(report(robot)));
    } catch (err) {
        console.error(chalk.red(`${err}`));
    } finally {
        console.info();
        rl.prompt();
    }
});

function printHints() {
    console.info(chalk.greenBright('Available commands are:'));
    console.info(`${PLACE_COMMAND} {x: number},{y: number},{direction: NORTH | EAST | SOUTH | WEST}`);
    console.info(MOVE_COMMAND);
    console.info(RIGHT_COMMAND);
    console.info(LEFT_COMMAND);
    console.info(REPORT_COMMAND);
    console.info(EXIT_COMMAND);
    console.info();
    console.info(chalk.greenBright('Please PLACE robot first'));
    console.info();
}
