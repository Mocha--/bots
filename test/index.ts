import {equal} from 'assert';
import {describe, it} from 'mocha';
import {Direction} from '../src/constants';
import {Robot} from '../src/models';
import {moveForward, turnLeft, turnRight, report, placeRobot} from '../src/utils';

function createDefaultRobot(): Robot {
    return {x: 0, y: 0, direction: null};
}

describe('PLACE Robot', () => {
    it('should not be placed out of the grid', () => {
        const robot = createDefaultRobot();
        equal(placeRobot(robot, 10, 10, Direction.North), robot);
    });

    it('should not be placed because of invalid direction', () => {
        const robot = createDefaultRobot();
        equal(placeRobot(robot, 0, 0, null), robot);
    });

    it('should be placed within the grid', () => {
        const robot = createDefaultRobot();
        const placed = placeRobot(robot, 3, 3, Direction.South);
        equal(report(placed), report({x: 3, y: 3, direction: Direction.South}));
    });
});

describe('MOVE Robot', () => {
    it('should stop at 3,2,EAST', () => {
        let robot = createDefaultRobot();
        robot = placeRobot(robot, 2, 2, Direction.East);
        robot = moveForward(robot);
        equal(report(robot), report({x: 3, y: 2, direction: Direction.East}));
    });

    it('should stop at 0,2,SOUTH', () => {
        let robot = createDefaultRobot();
        robot = placeRobot(robot, 0, 4, Direction.South);
        robot = moveForward(robot);
        robot = moveForward(robot);
        equal(report(robot), report({x: 0, y: 2, direction: Direction.South}));
    });

    it('should stop at 0,0,SOUTH', () => {
        let robot = createDefaultRobot();
        robot = placeRobot(robot, 0, 0, Direction.South);
        robot = moveForward(robot);
        equal(report(robot), report({x: 0, y: 0, direction: Direction.South}));
    });

    it('should stop at 4,4,NORTH', () => {
        let robot = createDefaultRobot();
        robot = placeRobot(robot, 3, 3, Direction.North);
        robot = turnRight(robot);
        robot = moveForward(robot);
        robot = turnLeft(robot);
        robot = moveForward(robot);
        equal(report(robot), report({x: 4, y: 4, direction: Direction.North}));
    });
});

describe('ROTATE Robot', () => {
    it('should face EAST', () => {
        let robot = createDefaultRobot();
        robot = placeRobot(robot, 3, 3, Direction.North);
        robot = turnLeft(robot);
        robot = turnLeft(robot);
        robot = turnLeft(robot);
        equal(report(robot), report({x: 3, y: 3, direction: Direction.East}));
    });

    it('should face WEST', () => {
        let robot = createDefaultRobot();
        robot = placeRobot(robot, 3, 3, Direction.North);
        robot = turnLeft(robot);
        robot = turnRight(robot);
        robot = turnLeft(robot);
        equal(report(robot), report({x: 3, y: 3, direction: Direction.West}));
    });
});
